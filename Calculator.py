import math

class Calculator():

	def __init__(self):
		pass

	def cos_(self, int1):
		self.result = math.cos(int(int1))

	def saknis(self, int1):
		self.result = math.sqrt(int(int1))
		return self.result

	def daugyba(self, int1, int2):
		self.result = int(int1) * int(int2)
		return self.result

	def dalyba(self, num1, num2):
		self.result = float(int(num1) / int(num2))
		return (self.result)

	def plus(self, int1, int2):
		self.result = int(int1) + int(int2)
		return self.result

	def minusas(self, int1, int2):
		self.result = int(int1) - int(int2)
		return self.result

	def kelimas(self, int1, int2):
		self.result = int(int1)**int(int2)
		return self.result

	def dalybosLikutis(self, int1, int2):
		self.result = (int(int1) % int(int2))
		return self.result

	def start(self):
		print("Galimi operatoriai: + - * / ** % cos sqrt")
		while True:
			str = input("Pasirinkti įvedus operatorių: ")
			if str=="+":
				self.plus(input("Įveskite reikšmę 1: "), input("Įveskite reikšmę 2: "))
			elif str=="-":
				self.minusas(input("Įveskite reikšmę 1: "), input("Įveskite reikšmę 2: "))
			elif str=="*":
				self.daugyba(input("Įveskite reikšmę 1: "), input("Įveskite reikšmę 2: "))
			elif str=="/":
				self.dalyba(input("Įveskite reikšmę 1: "), input("Įveskite reikšmę 2: "))
			elif str == "**":
				self.kelimas(input("Įveskite reikšmę 1: "), input("Kelintu laipsniu kelti? "))
			elif str == "%":
				self.dalybosLikutis(input("Įveskite reikšmę 1: "), input("Įveskite reikšmę 2: "))
			elif str == "cos":
				self.cos_(input("Įveskite reikšmę : "))
			elif str == "sqrt":
				self.saknis(input("Įveskite reikšmę : "))
			else:
				print("Nėra tokios reikšmės! \n")
			print(f"Rezultatas yra: {self.result}\n")

kalkuliatorius = Calculator()
kalkuliatorius.start()
